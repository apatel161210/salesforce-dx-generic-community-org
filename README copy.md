# Salesforce App


## system requrirements 
install sfdx
install VS code or intelleJ IDE


## Environment setup
IntelliJ -> New Project -> Illuminated Cloud SFDX
Enter project name, leave namespace blank, leave package directory name as "force-app"
Click Next then Finish
Choose an existing scratch org or create a new one if one doesn't exist
Create a new one by clicking the '+' symbol
Put in the org name and alias name
For Features, check Communities and Sites
For Org Preferences. check ChatterEnabled and NetworksEnabled
Leave default for scratch org definition file and click OK
Choose your new connection
Hit OK & let everything load. Restart IDEA afterwards.


## Create password
sfdx force:user:password:generate --targetusername <username> 

## Display org information 
sfdx force:user:display -u <username>

## Open an org
sfdx force:org:open -u <username>

## Authorize the org
sfdx force:auth:web:login -r <Login url>

## Configuration

 To create or update users for this profile, go to Setup > Communities Settings and -> select Allow using standard external profiles for self-registration and user creation.
 Need to create temporary role and assinged it to "User User" because sfdx orgs are not allowing existing role assignment to users. 


## Insert accounts & contacts

 sfdx force:data:tree:import -p <path to Account-contact.json file> -u <username>
 
## Deploy custom profile files into org

sfdx force:mdapi:deploy -u <username> -d <directory> -w <waitTimeInMinute>

## Run Apex script to enable users

sfdx force:apex:execute -f <path to apex file> -u <username>
 
## Resources

## Description of Files and Directories
 Insert and placed correclty mainfrest/package folder and script folder into your project. 

## Issues
Custom profiles are allowing upto three users in sfdx